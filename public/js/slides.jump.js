class S6_Plugin_Jump {

  constructor(deck, options) {

    window.addEventListener('load', ev => {
      var urlParams = new URLSearchParams(window.location.search);

      let jump = parseInt(urlParams.get('jump'));

      if (!jump) {
        return;
      }

      deck.step(jump);

    });
  }
} // class S6_Plugin_Jump



//////////////////////////////
// add global S6 "export"
//   e.g. lets you call jump( options ) for plugins array config

var S6 = S6 || {};
S6.jump = options => deck => new S6_Plugin_Jump(deck, options);
