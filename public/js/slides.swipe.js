
// todo: use class expression to "namespace"
//   S6.Plugins.Swipe  or S6.DeckSwipePlugin for now ???


class S6_Plugin_Swipe {

   constructor( deck, options ) {

    this.container = deck.parent;

    this.container.addEventListener("touchstart", this.startTouch, false);
    this.container.addEventListener("touchmove", this.moveTouch, false);

    // Swipe Up / Down / Left / Right
    this.initialX = null;
    this.initialY = null;    

    }

    startTouch(e) {
      this.initialX = e.touches[0].clientX;
      this.initialY = e.touches[0].clientY;
    };

    moveTouch(e) {
      if (this.initialX === null) {
        return;
      }

      if (this.initialY === null) {
        return;
      }

      let currentX = e.touches[0].clientX;
      let currentY = e.touches[0].clientY;

      let diffX = this.initialX - currentX;
      let diffY = this.initialY - currentY;

      if (Math.abs(diffX) > Math.abs(diffY)) {
        // sliding horizontally
        if (diffX > 0) {
          // swiped left
          deck.next()
        } else {
          // swiped right
          deck.prev();
        }  
      } else {
        // sliding vertically
        if (diffY > 0) {
          // swiped up
          //deck.prev();
        } else {
          // swiped down
          //deck.next()
        }  
      }

      this.initialX = null;
      this.initialY = null;

      e.preventDefault();
    };
} // class S6_Plugin_Swipe



//////////////////////////////
// add global S6 "export"
//   e.g. lets you call Swipe( options ) for plugins array config

var S6 = S6 || {};
S6.swipe = options => deck => new S6_Plugin_Swipe( deck, options );
